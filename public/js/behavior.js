(function(){


  $(function(){

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    /*localstorageのfavoを取得して初期かリピーターかを判別*/
    if(localStorage.getItem('favo')){
      //favoがある場合は配列として取り込み、favoに登録されているthreadにクラスをつける
      var favolist = JSON.parse(localStorage.getItem('favo'));
      favolist.forEach(function(value){
        var targetClass = ".thread__id" + value;
        $(targetClass).find(".user__data__favo").children("a").addClass("favoed");
      });
    }else{
      var favolist = new Array();
    }

    /*お気に入り機能の動作*/
    $(".user__data__favo a").click(function(){
      var thisLink = $(this);
      var threadId = $(this).parent().parent().parent().attr("name");

      /*お気に入りの判別*/
      if($(this).hasClass("favoed")){
        $.ajax({
          type: "POST",
          url: "/favo/remove",
          dataType : "json",
          data: {
            threadId: threadId
          }
        }).done(function(favoRe){
          var result = JSON.parse(favoRe);
          thisLink.parent().children('span').empty().html(result);
        }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
          console.log("ajax通信に失敗しました");
          console.log("XMLHttpRequest : " + XMLHttpRequest.status);
          console.log("textStatus     : " + textStatus);
          console.log("errorThrown    : " + errorThrown.message);
        });

        //お気に入りを解除した場合にlocalstorageも削除
        favolist.forEach((item, index) => {
            if(item === threadId) {
                favolist.splice(index, threadId);
            }
        });
        localStorage.removeItem("favo");
        localStorage.setItem("favo", JSON.stringify(favolist));

        $(this).removeClass("favoed");

      }else{
        $.ajax({
          type: "POST",
          url: "/favo/add",
          dataType : "json",
          data: {
            threadId: threadId
          }
        }).done(function(favoRe){
          var result = JSON.parse(favoRe);
          thisLink.parent().children('span').empty().html(result);
        }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
          console.log("ajax通信に失敗しました");
          console.log("XMLHttpRequest : " + XMLHttpRequest.status);
          console.log("textStatus     : " + textStatus);
          console.log("errorThrown    : " + errorThrown.message);
        });

        if(favolist.indexOf(threadId) == -1){
          favolist.push(threadId);
          localStorage.setItem("favo", JSON.stringify(favolist));
        }

        $(this).addClass("favoed");
      }

      return false;
    });

    function afterCheck(){
      var afterItem = localStorage.getItem('after');
      document.getElementById("afterArray").value = afterItem;
    }

    afterCheck();

    /*あとで読む*/
    if(localStorage.getItem('after')){
      //favoがある場合は配列として取り込み、favoに登録されているthreadにクラスをつける
      var afterList = JSON.parse(localStorage.getItem('after'));
      afterList.forEach(function(value){
        var targetClass = ".thread__id" + value;
        $(targetClass).find(".user__data__after").children("a").addClass("aftered");
      });
    }else{
      var afterList = new Array();
    }

    $(".user__data__after a").click(function(){
      var threadId = $(this).parent().parent().parent().attr("name");
      if($(this).hasClass("aftered")){
        afterList.forEach((item, index) => {
            if(item === threadId) {
                afterList.splice(index, threadId);
            }
        });
        localStorage.removeItem("after");
        localStorage.setItem("after", JSON.stringify(afterList));
        $(this).removeClass("aftered");
      }else{
        if(afterList.indexOf(threadId) == -1){
          afterList.push(threadId);
          localStorage.setItem("after", JSON.stringify(afterList));
        }
        $(this).addClass("aftered");
      }
      afterCheck();
      return false;
    });

    function makeToken(){
      // 生成する文字列の長さ
      var l = 8;

      // 生成する文字列に含める文字セット
      var c = "abcdefghijklmnopqrstuvwxyz0123456789";

      var cl = c.length;
      var r = "";
      for(var i=0; i<l; i++){
        r += c[Math.floor(Math.random()*cl)];
      }
      return r;
    }

    if(localStorage.getItem('token')){
      var token = localStorage.getItem('token');
      document.getElementById("setToken").value = token;
      document.getElementById("setTokenDef").value = token;
    }else{
      var token = makeToken();
      document.getElementById("setToken").value = token;
      document.getElementById("setTokenDef").value = token;
      localStorage.setItem("token",token);
    }

  });

}());

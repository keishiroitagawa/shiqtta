'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cmq = require('gulp-combine-media-queries');//@media
var sassGlob = require('gulp-sass-glob');
var prefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

gulp.task('sass', function () {
  return gulp.src('./resources/assets/sass/style.scss')
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix({
        browsers: ['last 2 version', 'iOS >= 8.1', 'Android >= 4.4'],
        cascade: false
    }))
    .pipe(minifyCSS({
      keepBreaks: false
    }))
    .pipe(gulp.dest('./public/css/'));
});

gulp.task('cmq', function () {
  gulp.src('./public/css/')
    .pipe(cmq({
      log: false
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('watch', function(){
    gulp.watch('./resources/assets/sass/*', ['sass']);
});

gulp.task('default', ['sass','cmq','watch']);

gulp.watch('./resources/assets/sass/*.scss',['sass','cmq']);

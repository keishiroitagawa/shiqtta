<!DOCTYPE HTML>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>@yield('title')</title>
  <link rel="stylesheet" href="/css/style.css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
 <div class="container">
 <header class="header">しくったー</header>
 <ul class="category__navi">
  <li><a href="/">新着</a></li>
  <li><a href="/?order=update">更新</a></li>
  <li><a href="/?order=hot">人気</a></li>
  <li>
    <form method="post" name="afterList" action="/after/list">
    {{ csrf_field() }}
    <input type="hidden" name="afterArray" value="" id="afterArray">
    <a href="javascript:afterList.submit()">あとで読む</a>
    </form>
  </li>
  <li>
    <form method="post" name="historyList" action="/history/list">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="" id="setTokenDef">
    <a href="javascript:historyList.submit()">履歴</a>
    </form>
  </li>
 </ul>
 @yield('content')
 </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script><!-- Scripts（Jquery） -->
 <script src="/js/behavior.js"></script>
 </body>
</html>

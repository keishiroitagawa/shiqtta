<!-- resources/views/index.blade.php -->

@extends('layouts.template')
@inject('fuzzytime', 'App\Libs\fuzzytimeClass')
@section('title', 'Shiqtta')
@section('content')

<div class="content">
  <form action="/" method="POST" class="main__post">
    {{ csrf_field() }}
    <textarea value="" name="text"></textarea>
    <input type="hidden" name="use_response_flg" value="1">
    <input type="hidden" name="token" value="" id="setToken">
    <input type="submit" value="つぶやく" class="send__btn">
  </form>
  @foreach($threads as $thread)
  <div name="{{$thread->id}}" class="data__thread__item thread__id{{$thread->id}}">
    <div class="data__message"><a href="/{{$thread->id}}">{{$thread->text}}</a></div>
    <dl class="user__data">
      <dt class="user__data__id">id: </dt>
      <dd class="user__daata__token">{{$thread->token}}</dd>
      <dd class="user__data__time">{{ $fuzzytime->convert_to_fuzzy_time($thread->created_at) }}</dd>
      <dd class="user__data__favo"><span>{{$thread->favorit}}</span><a href="#">どんまい!</a></dd>
      <dd class="user__data__comment"><a href="{{$thread->id}}"><span>{{$thread->comment}}件のコメント</span></a></dd>
      <dd class="user__data__after"><a href="#">あとで読む</a></dd>
    </dl>
  </div>
  @endforeach
@endsection

<!-- resources/views/index.blade.php -->

@extends('layouts.template')
@inject('fuzzytime', 'App\Libs\fuzzytimeClass')
@section('title', 'Shiqtta')
@section('content')

<div class="content">
  コメント：{{$item['text']}}<br>
  ID：{{$item['token']}}

  <div class="textbox">
    <form action="/{{$item['id']}}" method="POST" class="main__post">
      {{ csrf_field() }}
      <textarea value="" name="text"></textarea>
      <input type="hidden" name="use_response_flg" value="2">
      <input type="hidden" name="resopnse_thread" value="{{$item['token']}}">
      <input type="hidden" name="resopnse_thread_id" value="{{$item['id']}}">
      <input type="hidden" name="token" value="" id="setToken">
      <input type="submit" value="匿名で返信する" class="send__btn">
    </form>
  </div>

  @foreach($threads as $thread)
  <div class="data__thread__item thread__id{{$thread->id}}">
    <div class="data__message">{{$thread->text}}</div>
    <dl class="user__data">
      <dt class="user__data__id">id: </dt>
      <dd class="user__daata__token">{{$thread->token}}</dd>
      <dd class="user__data__time">{{ $fuzzytime->convert_to_fuzzy_time($thread->created_at) }}</dd>
      <dd class="user__data__favo"><span>{{$thread->favorit}}</span><a href="#">どんまい!</a></dd>
      <dd class="user__data__comment"><a href="#">コメント</a></dd>
    </dl>
  </div>
  @endforeach

@endsection

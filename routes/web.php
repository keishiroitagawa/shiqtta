<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/','ShiqttaController@index');
Route::post('/','ShiqttaController@post');
Route::get('/{id}','ShiqttaController@detail');
Route::post('/{id}','ShiqttaController@detailPost');
Route::post('/favo/add','ShiqttaController@favoAdd');
Route::post('/favo/remove','ShiqttaController@favoRemove');
Route::post('/after/list','ShiqttaController@after');
Route::post('/history/list','ShiqttaController@history');

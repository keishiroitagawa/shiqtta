<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
  protected $guarded = array('id');
  public static $rules = array(
    'text' => 'required',
    'token' => 'required',
    'thread_id' => 'required',
    'use_response_flg' => 'required',
  );
}

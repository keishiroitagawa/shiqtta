<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Thread;

class ShiqttaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if($request->order == 'update'){
        /* 更新日時順で表示 */
        $threads = Thread::orderBy('updated_at','DESC')->where('use_response_flg','1')->get();
      }else if($request->order == 'hot'){
        /*人気スレッド*/
        $threads = Thread::orderBy('comment','DESC')->where('use_response_flg','1')->get();
      }else{
        /* 通常表示 */
        $threads = Thread::orderBy('id','DESC')->where('use_response_flg','1')->get();
      }
      /* viewindexを使用して表示 */
      return view('index',compact('threads'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'text' => 'required|max:400'
      ]);

      if ($validator->fails()) {
          return redirect('/')
              ->withInput()
              ->withErrors($validator);
      }

      /*トークンセット*/
      $uniqId =  $request->token;

      /*つぶやき登録*/
      $thread = new Thread;
      $thread->text = $request->text;
      $thread->token = $uniqId;
      $thread->thread_id = '';
      $thread->use_response_flg = $request->use_response_flg;
      $thread->save();
      return redirect('/');
    }

    public function detail($id)
    {
      $threads = Thread::orderBy('id','DESC')->where('thread_id',$id)->get();
      $item = Thread::where('id', $id)->first();
      return view('detail',compact('item','threads'));
    }

    public function detailPost(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'text' => 'required|max:400'
      ]);

      /*トークン作成*/
      $uniqId =  $request->token;

      $id = $request -> resopnse_thread_id;
      $resThreadId = Thread::findOrFail($id);
      $resThreadComNum = $resThreadId->comment;
      $resThreadComNum = $resThreadComNum + 1;
      $resThreadId->comment = $resThreadComNum;
      $resThreadId->save();

      $thread = new Thread;
      $thread->text = $request->text;
      $thread->token = $uniqId;
      $thread->thread_id = $id;
      $thread->use_response_flg = $request->use_response_flg;
      $thread->save();

      $nowDate = date("Y/m/d H:i:s");
      Thread::where('id',$id)->update(['updated_at'=>$nowDate]);

      $item = Thread::where('id', $id)->first();

      /* 新着順でスレッドを取得 */
      $threads = Thread::orderBy('id','DESC')->where('thread_id',$id)->get();
      return view('detail',compact('item','threads'));

    }

    public function favoAdd(Request $request)
    {
      $threadId = $request -> threadId;
      $favoThreadId = Thread::findOrFail($threadId);
      $favoThreadFavoNum = $favoThreadId->favorit;
      $favoThreadFavoNum = $favoThreadFavoNum + 1;
      $favoThreadId->favorit = $favoThreadFavoNum;
      $favoThreadId->timestamps = false;
      $favoThreadId->save();
      echo json_encode("$favoThreadFavoNum");
    }

    public function favoRemove(Request $request)
    {
      /*test*/
      $threadId = $request -> threadId;
      $favoThreadId = Thread::findOrFail($threadId);
      $favoThreadFavoNum = $favoThreadId->favorit;
      $favoThreadFavoNum = $favoThreadFavoNum - 1;
      $favoThreadId->favorit = $favoThreadFavoNum;
      $favoThreadId->timestamps = false;
      $favoThreadId->save();
      echo json_encode("$favoThreadFavoNum");
    }

    public function after(Request $request)
    {
      $afterArray = $request -> afterArray;
      $afterArray = str_replace('"', '', $afterArray);
      $afterArray = str_replace('[', '', $afterArray);
      $afterArray = str_replace(']', '', $afterArray);
      $afterArray = explode(',',$afterArray);
      $threads = Thread::orderBy('id','DESC')->where('use_response_flg','1')->whereIn('id',$afterArray)->get();

      return view('index',compact('threads'));
    }

    public function history(Request $request)
    {
      $historyToken = $request -> token;
      $threads = Thread::orderBy('id','DESC')->where('use_response_flg','1')->where('token',$historyToken)->get();
      return view('index',compact('threads'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

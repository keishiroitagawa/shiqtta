<?php

function makeToken(){
  list($msec, $sec) = explode(" ", microtime());
  $hashCreateTime = $sec.floor($msec*1000000);
  $hashCreateTime = strrev($hashCreateTime);
  $uniqId = base_convert($hashCreateTime,10,36);
  return $uniqId;
}
